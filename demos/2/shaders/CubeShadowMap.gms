#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

out vec4 FragPos;
uniform mat4 VPM[6];

void main()
{
	for(int CubeFace = 0;CubeFace < 6;CubeFace++)
	{
		gl_Layer = CubeFace;
		for(int i = 0;i < 3;i++)
		{
			FragPos = gl_in[i].gl_Position;
			gl_Position = VPM[CubeFace] * FragPos;
			EmitVertex();
		}
		EndPrimitive();
	}
}