#include "Sphere.h"
#include <cmath>
#include "Material.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
Sphere::Sphere(int rings, int segments)
{
    float const R = (float)1.0f/(float)(rings - 1);
    float const S = (float)1.0f/(float)(segments - 1);
    vertices.resize(rings * segments *3);
    indices.resize(rings * segments * 4);
    for(int r = 0; r < rings; r++) for(int s = 0; s < segments; s++)
    {
        vec3 temp = vec3(sin( -M_PI_2 + M_PI * r * R ),
                         cos(2*M_PI * s * S) * sin( M_PI * r * R ),
                         sin(2*M_PI * s * S) * sin( M_PI * r * R ) );
        vertices[r * rings + s] = temp.x;
        vertices[(r * rings + s) + 1] = temp.y;
        vertices[(r * rings + s) + 2] = temp.z;
    }
    int i = 0;
    for(int r = 0; r < rings-1; r++) for(int s = 0; s < segments-1; s++)
    {
        indices[i++] = r * segments + s;
        indices[i++] = r * segments + (s+1);
        indices[i++] = (r+1) * segments + (s+1);
        indices[i++] = (r+1) * segments + s;
    }
     glGenVertexArrays(1, &VertexArrayID);
     glBindVertexArray(VertexArrayID);

     glGenBuffers(1, &vertexbuffer);
     glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
     glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

     glGenBuffers(1, &elementbuffer);
     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
     glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
}

void Sphere::SetRad(float _Rad)
{
    radious = _Rad;
}

void Sphere::SetPos(vec3 Position_)
{
    Position = Position_;
}

void Sphere::Draw(mat4 MVP, GLuint ProgramID)
{
    mat4 SMVP = glm::translate(MVP, Position);
    SMVP = glm::scale(SMVP,vec3(radious));
    GLuint MVPid = glGetUniformLocation(ProgramID, "MVP");
    glUniformMatrix4fv(MVPid, 1, GL_FALSE, &SMVP[0][0]);
    glBindVertexArray(VertexArrayID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
       0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
       3,                  // size
       GL_FLOAT,           // type
       GL_FALSE,           // normalized?
       0,                  // stride
       (void*)0            // array buffer offset
    );
    glDrawElements(
             GL_QUADS,      // mode
             indices.size(),    // count
             GL_UNSIGNED_INT,   // type
             (void*)0           // element array buffer offset
           );


}

Sphere::~Sphere()
{
    //dtor
}
