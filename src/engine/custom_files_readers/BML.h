#include"main.h"

struct BML
{
    string name;
    int vertices_count, faces_count;
    vec3 * vertices;
    vec3 * normals;
    vec2 * uv;
    vec2 * lightmap_uv;
    ivec3 * indices;
};

void load_BML(const string file_path, BML &outdata);
