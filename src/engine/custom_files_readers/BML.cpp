#include <BML.h>
#include <iomanip>
void load_BML(const string file_path, BML &outdata)
{
    ifstream input_BML;
    input_BML.open(file_path.c_str(), ios::binary | ios::in);

    //checks signature
    char signature, correct_signaure = 0xF9;
    input_BML.read(&signature, 1);
    if(signature != correct_signaure)
    {
        cerr << "unmatching signature, not correct bml" << endl;
        cout << "signature is :"<< hex << (int) signature << endl;
        return;
    }

    //reads name

    //reads name_lenght
    int16_t name_lenght;
    input_BML.read((char*)&name_lenght, 2);
    //reads name in a temporary string
    char* temp_name = new char[name_lenght];
    input_BML.read(temp_name, name_lenght);
    //stores it in the output structure
    outdata.name = temp_name;
    delete(temp_name);

    //checks for error
    if(input_BML.fail())
    {
        cerr << "fail returned true" << endl;
        if(input_BML.eof())
        {
            cerr << "eof" << endl;
        }
        /*cerr << "eof" << input_BML.eof() ? "true" : "false" << endl;
        cerr << "bad" << input_BML.bad() ? "true" : "false" << endl;*/
        return;
    }
    //reads vertices count
    input_BML.read((char*)&outdata.vertices_count, 4);
    //reads faces count
    input_BML.read((char*)&outdata.faces_count, 4);

    //read vertices
    outdata.vertices = new vec3[outdata.vertices_count];
    input_BML.read((char*)outdata.vertices, outdata.vertices_count * 4 * 3);

    //read normals
    outdata.normals = new vec3[outdata.vertices_count];
    input_BML.read((char*)outdata.normals, outdata.vertices_count * 4 * 3);

    //read uv
    outdata.uv = new vec2[outdata.vertices_count];
    input_BML.read((char*)outdata.uv, outdata.vertices_count * 4 * 2);

    //read lightmap uv
    outdata.lightmap_uv = new vec2[outdata.vertices_count];
    input_BML.read((char*)outdata.lightmap_uv, outdata.vertices_count * 4 * 2);

    //read indices
    outdata.indices = new ivec3[outdata.faces_count];
    input_BML.read((char*)outdata.indices, outdata.faces_count * 4 * 3);

    //checks for error
    if(input_BML.fail())
    {
        cerr << "fail returned true" << endl;
        return;
    }

}
