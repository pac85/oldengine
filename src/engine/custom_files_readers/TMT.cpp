#include"TMT.h"
#include <sstream>

void process_keywords(string in_str, TMT &outdata)
{
	stringstream temp_ss(in_str);
	string string_popper;

	temp_ss >> string_popper;

	if(string_popper == "-frag")
	{
		temp_ss >> outdata.fragment_shader_path;

		return;
	}

	if(string_popper == "-vert")
	{
		temp_ss >> outdata.vertex_shader_path;

		return;
	}

	if(string_popper == "-diff")
    {
        string sub_popper;
        temp_ss >> sub_popper;
        if(sub_popper == "-text")
        {
            outdata.diff_text = true;
            temp_ss >> outdata.diffuse_texname;
            return;
        }
        if(sub_popper == "-unif")
        {
            outdata.diff_text = false;
            float x_pop, y_pop, z_pop;
            temp_ss >> x_pop>> y_pop>> z_pop;
            outdata.diffuse_color = vec3(x_pop, y_pop, z_pop);
            return;
        }

    }

    if(string_popper == "-spec")
    {
        string sub_popper;
        temp_ss >> sub_popper;
        if(sub_popper == "-text")
        {
            outdata.spec_text = true;
            temp_ss >> outdata.specular_texname;
            return;
        }
        if(sub_popper == "-unif")
        {
            outdata.spec_text = false;
            float x_pop, y_pop, z_pop;
            temp_ss >> x_pop>> y_pop>> z_pop;
            outdata.specular_color = vec3(x_pop, y_pop, z_pop);
            return;
        }

    }

    if(string_popper == "-rough")
    {
        string sub_popper;
        temp_ss >> sub_popper;
        if(sub_popper == "-text")
        {
            outdata.roug_text = true;
            temp_ss >> outdata.rough_textname;
            return;
        }
        if(sub_popper == "-unif")
        {
            outdata.roug_text = false;
            temp_ss >> outdata.roughness;
            return;
        }

    }

    if(string_popper == "-emi")
    {
        string sub_popper;
        temp_ss >> sub_popper;
        if(sub_popper == "-text")
        {
            outdata.emi_text = true;
            temp_ss >> outdata.emission_texname;
            return;
        }
        if(sub_popper == "-unif")
        {
            outdata.emi_text = false;
            float x_pop, y_pop, z_pop;
            temp_ss >> x_pop>> y_pop>> z_pop;
            outdata.emission_color = vec3(x_pop, y_pop, z_pop);
            return;
        }

    }

    if(string_popper == "-d/m")
    {
        string sub_popper;
        temp_ss >> sub_popper;
        if(sub_popper == "-text")
        {
            outdata.dielectric_metal_texture = true;
            temp_ss >> outdata.dielectric_metal_texname;
            return;
        }
        if(sub_popper == "-unif")
        {
            outdata.dielectric_metal_texture = false;
            temp_ss >> outdata.dielectric_metal;
            return;
        }

    }

    if(string_popper == "-norm")
    {
        string sub_popper;
        temp_ss >> sub_popper;
        if(sub_popper == "-text")
        {
            outdata.norm_text = true;
            temp_ss >> outdata.diffuse_texname;
            return;
        }
    }

    if(string_popper == "-ior")
    {
        string sub_popper;
        temp_ss >> sub_popper;

        if(sub_popper == "-unif")
        {
            temp_ss >> outdata.ior;
            return;
        }
    }

    if(string_popper == "-trasm")
    {
        string sub_popper;
        temp_ss >> sub_popper;

        if(sub_popper == "-unif")
        {
            temp_ss >> outdata.transmittance;
            return;
        }
    }

    if(string_popper == "-trasp")
    {
        string sub_popper;
        temp_ss >> sub_popper;

        if(sub_popper == "-unif")
        {
            temp_ss >> outdata.transparency;
            return;
        }
    }
}

void load_TMT(string file_path, TMT &outdata)
{
    ifstream input_TMT;
    input_TMT.open(file_path.c_str(), ios::in);

    string line;
    if(input_TMT.is_open())
    {
        while( getline(input_TMT, line) )
        {
            process_keywords(line, outdata);
        }
        input_TMT.close();
    }
}
