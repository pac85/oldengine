#include"main.h"
enum e_shader_type
{
    frag,
    vert
};

enum PBR_type
{
    diff,
    spec,
    rough,
    emi,
    die_met,
    norm,
    ior,
    trasm,
    trasp
};

enum PBR_data_type
{
    text,
    unif
};

struct property
{
    e_shader_type shader_type;
    PBR_type pbr_types;
    PBR_data_type PBR_data_types;
    string value;
};

struct TMT
{
    string fragment_shader_path;
    string vertex_shader_path;

    bool diff_text;                     //if true diffuse is a texture

    string diffuse_texname;
    vec3 diffuse_color;


    bool spec_text;                     //if true specular is a texture

    string specular_texname;
    vec3 specular_color;


    bool roug_text;                     //if true roughness is a texture

    string rough_textname;
    float roughness;


    bool emi_text;                      //if true emission is a texture

    string emission_texname;
    vec3 emission_color;


    bool dielectric_metal_texture ;     //if true dielectric_metal is a texture

    string dielectric_metal_texname;
    float dielectric_metal;


    float transparency;
    float ior;
    float transmittance;

    bool norm_text;                     //if true there is a normal map
    string normal_text_name;

    struct custom_prop
    {
        enum type
        {
            integer,
            floating,
            vec2,
            vec3,
            vec4,
            ivec2,
            ivec3,
            ivec4
        };

        int c_int;
        float c_float;
        glm::vec2 c_vec2;
        glm::vec3 c_vec3;
        glm::vec4 c_vec4;
        glm::ivec2 c_ivec2;
        glm::ivec3 c_ivec3;
        glm::ivec4 c_ivec4;

    };

    map<string, custom_prop> custom_props;

};

void load_TMT(string file_path, TMT outdata);

