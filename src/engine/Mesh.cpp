#include"Mesh.h"
#include"custom_files_readers/BML.h"

using namespace tinyobj;



/*void Model::Load_Obj(std::string File,GLenum DrawType)
{
	//Loads the obj in Mesh and materials vectors
	std::cout <<"loading "<< File << std::endl;
	std::string loadingErrors = LoadObj(Mesh, materials, File.c_str());
	if (!loadingErrors.empty())
	{
		std::cout << loadingErrors << std::endl;
		return;
	}
	MeshCount = Mesh.size();
	std::cout << File << " has " << MeshCount << " meshes " << std::endl;
	//for each mesh generates various buffers
	for (unsigned int mi = 0; mi < Mesh.size(); mi++)
	{
		//generates VAO
		GLuint VAOTemp;
		glGenVertexArrays(1, &VAOTemp);
		glBindVertexArray(VAOTemp);
		VAO.push_back(VAOTemp);
		glBindVertexArray(0);
		//generates VBO
		GLuint VBOTemp;
		glGenBuffers(1, &VBOTemp);
		glBindBuffer(GL_ARRAY_BUFFER, VBOTemp);
		glBufferData(GL_ARRAY_BUFFER, Mesh[mi].mesh.positions.size() * sizeof(float), &Mesh[mi].mesh.positions[0], DrawType);
		VBO.push_back(VBOTemp);
		//generates UVBuffer
		GLuint UVBTemp;
		glGenBuffers(1, &UVBTemp);
		glBindBuffer(GL_ARRAY_BUFFER, UVBTemp);
		glBufferData(GL_ARRAY_BUFFER, Mesh[mi].mesh.texcoords.size() * sizeof(float), &Mesh[mi].mesh.texcoords[0], DrawType);
		UVB.push_back(UVBTemp);
		//Generates normal buffer object
		GLuint NBOTemp;
		glGenBuffers(1, &NBOTemp);
		glBindBuffer(GL_ARRAY_BUFFER, NBOTemp);
		glBufferData(GL_ARRAY_BUFFER, Mesh[mi].mesh.normals.size() * sizeof(float), &Mesh[mi].mesh.normals[0], DrawType);
		NBO.push_back(NBOTemp);
		//generates index buffer object
		GLuint IBOTemp;
		glGenBuffers(1, &IBOTemp);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBOTemp);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, Mesh[mi].mesh.indices.size() * sizeof(unsigned int), &Mesh[mi].mesh.indices[0], DrawType);
		IBO.push_back(IBOTemp);
		//unbinds buffers
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		//saves mesh's name
		Meshname.push_back(Mesh[mi].name);
		meshmap[Mesh[mi].name] = mi;
		//std::cout << File  <<"'s " << Meshname[mi] << " loaded in video memory" << std::endl;
	}
	std::cout << File << " loaded" << std::endl;
}*/

void Mesh::Load_BML(std::string File, GLenum DrawType)
{
    cout << "loading: " << File << "..." << endl;
    BML bml_data;
    load_BML(File, bml_data);  //loads the bml file

    name = bml_data.name;                           //stores the mesh name in the member variable
    vertices_count = bml_data.vertices_count;       //stores vertices count in the member variable
    faces_count = bml_data.faces_count;             //stores faces count in the member variable

    glGenVertexArrays(1, &VAO); //Genrates vertex array object

    //generates vertex buffer object and fills it
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, bml_data.vertices_count * sizeof(vec3), bml_data.vertices, DrawType);

    //generates normal buffer object and fills it
    glGenBuffers(1, &NBO);
    glBindBuffer(GL_ARRAY_BUFFER, NBO);
    glBufferData(GL_ARRAY_BUFFER, bml_data.vertices_count * sizeof(vec3), bml_data.normals, DrawType);

    //generates uv buffer object and fills it
    glGenBuffers(1, &UVB);
    glBindBuffer(GL_ARRAY_BUFFER, UVB);
    glBufferData(GL_ARRAY_BUFFER, bml_data.vertices_count * sizeof(vec2), bml_data.uv, DrawType);

    //generates lightmap uv buffer object and fills it
    glGenBuffers(1, &LUVB);
    glBindBuffer(GL_ARRAY_BUFFER, LUVB);
    glBufferData(GL_ARRAY_BUFFER, bml_data.vertices_count * sizeof(vec2), bml_data.lightmap_uv, DrawType);

    //generates index buffer and fills it
    glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, bml_data.faces_count * sizeof(ivec3), bml_data.indices, DrawType);

	cout << File << " loaded" << endl;
}


void Mesh::DrawAll()
{

    //binds vao
    glBindVertexArray(VAO);


    //binds vbo uvb and nbo and sets them as 0st 1st and 2nd attributes
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);


    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, UVB);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);


    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, NBO);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);



    glDrawElements(GL_TRIANGLES, faces_count, GL_UNSIGNED_INT, (void*)0);


    //unbinds buffers
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

