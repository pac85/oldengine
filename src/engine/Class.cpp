#include "Class.h"

/**
These function help to create actor instances by an assigned name.
This is really helpful since when loading levels only a string containing the actor name i known.

This stack overflow thread was really helpful:
http://stackoverflow.com/questions/582331/is-there-a-way-to-instantiate-objects-from-a-string-holding-their-class-name
*/


template<typename T> Actor * InstanciateActor();
{
    return new T;
}

