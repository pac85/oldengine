#ifndef POINTLIGHT_H
#define POINTLIGHT_H
#include"main.h"
#include"GBuffer.h"
#include"Sphere.h"
#include"ShadowMap.h"
#include"Actor.h"


class PointLight : public Actor
{
    public:
        PointLight();
        void Initial_params(GLuint , GLuint , unsigned int, bool);
        virtual ~PointLight();
        void set_position(vec3 _position);
        void SetIntensity(vec3 Int);
        vec3 GetIntensity();
        void DrawLight(GBuffer InputGBuffer, mat4 MVP);
        void SetSourceRad(float Rad);
        void SetSourceLength();
        void SetClampRad(float ClampRad_);
        ShadowMap * getShadowMap();
        void BindShadowMapForDrawing(unsigned int face, GLuint program);

    protected:

    private:
        //The registration macro witch must be in every actor derived class
        REGISTER_DEC_TYPE(PointLight);

        vec3 Intensity;
        float SourceRad, SorceLength, ClampRad;
        bool bReady,        //set to true only once Initial_params is called
            bShadows;

        Sphere PLightBSphere;
        ShadowMap * lShadowMap;

        GLuint PointLightProgram, SphereProg, PositionID, IntensityID, SourceRadID;
        uint id;

        void tick(float delta);
        void GameStart();
};
#endif // POINTLIGHT_H



