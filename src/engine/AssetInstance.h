#pragma once
#include"main.h"
#include"Mesh.h"
#include"Material.h"


class Asset_instance
{
    private:
        Material * material;
        Mesh * model;
        mat4 MVP;
    public:
        Asset_instance(Mesh *,Material *);
        Asset_instance(Mesh *);
        Mesh * GetModel();
        Material * getMaterial();
        void Asset_DrawAll();
        void SetMVP(mat4 MVPp);
        mat4 getMVP();

};
