#pragma once
#include"main.h"
class GBuffer
{
private:
    GLuint GBufferTextures[9], testTexture, FinalImage, Stencil;
    GLuint GFrameBuffer;
	bool bIsOk;
	string GBuffertexturesNames[9];
	GLuint Quad,  quadArray;
	int WHeight, WWidth;
public:
	GBuffer(int, int);
	void BindGBufferForDrawing();
	void BindGBufferForReading();
	void BindGBufferForLighting(GLuint Program);
	void DrawGBuffer(GLuint target, int Width, int Height, GLuint Program);
	void DrawFinalBuffer();
	void CleanGBuffer();


};
