#ifndef SPOTLIGHT_H
#define SPOTLIGHT_H
#include"main.h"
#include"ShadowMap.h"
#include"Cone.h"
#include"GBuffer.h"
#include"Actor.h"

class SpotLight : public Actor
{
    public:
        SpotLight();
        void Initial_params(GLuint , GLuint , unsigned int, bool);

        void BindShadowMapForDrawing(GLuint);
        void DrawLight(GBuffer InputGBuffer, mat4 MVP);

        void set_position(vec3 _position);
        void set_rotation(vec3 _rotation);
        void SetIntensity(vec3 _Intensity);
        void SetSourceRad(float _SourceRad);
        void SetConeLength(float _ConeLength);
        void SetConeAngle(float _ConeAngle);
        void SetInternalConeAngle(float _IConeAngle);

        virtual ~SpotLight();

    protected:

    private:
        //The registration macro witch must be in every actor derived class
        REGISTER_DEC_TYPE(SpotLight);

        bool bReady,        //set to true only once Initial_params is called
            bShadows;
        ShadowMap * lShadowMap;
        GLuint SpotLightProgram, ConeProgram, MVPid, VPMid,
         PositionID, IntensityID, SourceRadID, DirectionID, ConeAngleCosID, IConeAngleCosID, ConeAngleTan;
        vec3 Intensity, Direction, UpVec;
        mat4 VPM, ConeMatrix;
        float SourceRad, ConeLength, ConeAngle, IConeAngle;
        Cone BoundingCone;


        void tick(float delta);
        void GameStart();

};

#endif // SPOTLIGHT_H
