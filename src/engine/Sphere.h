#ifndef SPHERE_H
#define SPHERE_H
#include"main.h"

class Sphere
{
    public:
        Sphere(int,int);
        virtual ~Sphere();
        void Draw(mat4 MVP, GLuint ProgramID);
        void SetRad(float _Rad);
        void SetPos(vec3 Position_);

    protected:

    private:
        float radious;
        vec3 Position;
        vector<float> vertices;
        vector<unsigned int> indices;
        GLuint VertexArrayID, vertexbuffer, elementbuffer;
};

#endif // SPHERE_H
