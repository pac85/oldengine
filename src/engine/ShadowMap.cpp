#include "ShadowMap.h"

const FaceInfo FacesInfo[6]=
{
    { GL_TEXTURE_CUBE_MAP_POSITIVE_X, vec3(1.0f, 0.0f, 0.0f),  vec3(0.0f, -1.0f, 0.0f) },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_X, vec3(-1.0f, 0.0f, 0.0f), vec3(0.0f, -1.0f, 0.0f) },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_Y, vec3(0.0f, 1.0f, 0.0f),  vec3(0.0f, 0.0f, 1.0f) },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, vec3(0.0f, -1.0f, 0.0f), vec3(0.0f,  0.0f, -1.0f) },
    { GL_TEXTURE_CUBE_MAP_POSITIVE_Z, vec3(0.0f, 0.0f, 1.0f),  vec3(0.0f, -1.0f, 0.0f) },
    { GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, vec3(0.0f, 0.0f, -1.0f), vec3(0.0f, -1.0f, 0.0f) }
};

ShadowMap::ShadowMap(float _Res, bool _bCubeMap = false)
{
    Res = _Res, bCubeMap = _bCubeMap;

    //Generates the shadow map's FBO and the shadow map cube map
    glGenFramebuffers(1, &ShadowMapFBO);
    glGenTextures(1, &ShadowMapTex);

    if(bCubeMap)
    {
        glBindTexture(GL_TEXTURE_CUBE_MAP, ShadowMapTex);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        /*glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);*/
        for(unsigned int i = 0;i < 6;i++)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_R32F, Res, Res, 0, GL_RED, GL_FLOAT, NULL);
        }


        //Separate render buffer for the depth buffer.It needs to be separate since the shadow map depth buffer contains linear depth
        glGenRenderbuffers(1, &DepthRB);
        glBindRenderbuffer(GL_RENDERBUFFER, DepthRB);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, Res, Res);

        glBindFramebuffer(GL_FRAMEBUFFER, ShadowMapFBO);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthRB);
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, ShadowMapTex);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Res, Res, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

        glBindFramebuffer(GL_FRAMEBUFFER, ShadowMapFBO);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, ShadowMapTex, 0);

    }


    //Checks the just created FBO
    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FB error, status: 0x%x\n", Status);
    }

    //Cleaning stuff
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

}

ShadowMap::~ShadowMap()
{
    //dtor
}

void ShadowMap::UseShadowMap(GLuint Program)
{
    //Binds the texture ans assigns it to the "ShadowCubeMap" uniform in the shader
    GLuint TexID = glGetUniformLocation(Program, bCubeMap ? "ShadowCubeMap" : "ShadowMap");
    if(!bCubeMap)
    {
        GLuint nc_TexID = glGetUniformLocation(Program, "nc_ShadowMap");
        glUniform1i(nc_TexID, 11);
    }

	glUniform1i(TexID, 10);
    glActiveTexture(GL_TEXTURE10);
    if(bCubeMap)
    {
        glBindTexture(GL_TEXTURE_CUBE_MAP, ShadowMapTex);
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, ShadowMapTex);
        glActiveTexture(GL_TEXTURE11);
        glBindTexture(GL_TEXTURE_2D, ShadowMapTex);
    }

}

mat4 ShadowMap::GetVM(unsigned int CubeFace)
{
        //Generates the projection and view matrices based on the CubeFace variable value

        //The FOV isn't 90 because it caused artifacts in the cubemap's edges.Seems like 89.535 produces a perfect 90� FOV matrix
        mat4 mProjection = glm::perspective(89.535f, 1.0f, 10.0f, fClip);
        mat4 mView = lookAt(pos, pos + FacesInfo[CubeFace].Target, FacesInfo[CubeFace].upVec);
        mat4 VP = mProjection * mView;
        return VP;
}

void ShadowMap::BindShadowMapForDrawing(vec3 Position, float NearClip, float FarClip, unsigned int face, GLuint ShadowProgram)
{
    //only to be used for cube map drawing!
    if(!bCubeMap){return;}
    //Binds the FBO so we can render to it

    nClip = NearClip, fClip = FarClip, pos = Position;

    glViewport(0, 0, Res, Res);
    glBindFramebuffer(GL_FRAMEBUFFER, ShadowMapFBO);
    glUseProgram(ShadowProgram);
    if(bCubeMap)
    {
        //Attaches the right texture based on the face variable value
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, FacesInfo[face].CubeFace, ShadowMapTex, 0);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        GLuint VPMIndex  =  glGetUniformLocation(ShadowProgram, "VPM"),
                            FarPlaneIndex = glGetUniformLocation(ShadowProgram, "FPlane"),
                            PositionIndex = glGetUniformLocation(ShadowProgram, "LightPosition");


        mat4 TempVPM = GetVM(face);
        glUniformMatrix4fv(VPMIndex, 1, GL_FALSE, &TempVPM[0][0]);
        glUniform1f(FarPlaneIndex, fClip);
        glUniform3fv(PositionIndex , 1, value_ptr(pos));
    }


    glClearColor(10000.0f,.0f,.0f, 0.0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
}

void ShadowMap::BindShadowMapForDrawing(mat4 VPM, GLuint ShadowProgram)
{
    if(bCubeMap){return;}

    glViewport(0, 0, Res, Res);
    glBindFramebuffer(GL_FRAMEBUFFER, ShadowMapFBO);
    glUseProgram(ShadowProgram);

    GLuint VPMIndex  =  glGetUniformLocation(ShadowProgram, "VPM");
    glUniformMatrix4fv(VPMIndex, 1, GL_FALSE, &VPM[0][0]);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    glClear(GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
}
