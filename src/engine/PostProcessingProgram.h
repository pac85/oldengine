#ifndef POSTPROCESSINGPROGRAM_H
#define POSTPROCESSINGPROGRAM_H
#include"main.h"
#include"PointLight.h"

class PostProcessingProgram
{
    public:
        PostProcessingProgram();
        virtual ~PostProcessingProgram();
        void UseProgram();
        void MakeShader(string VertexFile, string FragmentFile);

    protected:

    private:
        GLuint Program;
};

#endif // POSTPROCESSINGPROGRAM_H
