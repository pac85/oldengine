#ifndef CONE_H
#define CONE_H
#include"main.h"

class Cone
{
    public:
        Cone(int);
        void setSize(float r1, float Angle, float Depth);
        void draw();
        virtual ~Cone();

    protected:

    private:
        bool bCanDraw;
        int Polygons;
        vector<vec3> circle_vertices;
        GLuint  Quad, QuadVAO, up_circleVBO, low_circleVBO, coneVBO, VAO;

};

#endif // CONE_H
