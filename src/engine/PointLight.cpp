#include "PointLight.h"

//The registration macro witch must be in every actor derived class
REGISTER_DEF_TYPE(PointLight);

PointLight::PointLight() : PLightBSphere(10, 10)
{

}

void PointLight::Initial_params(GLuint _PointLightProgram, GLuint _SphereProg, unsigned int ShadowMapRes, bool _bShadows)
{
    PointLightProgram = _PointLightProgram, bShadows = _bShadows, SphereProg = _SphereProg;
    if(bShadows)
    {
        lShadowMap = new ShadowMap(ShadowMapRes, true);
    }

    PositionID  = glGetUniformLocation(PointLightProgram, "light_pos");
    IntensityID = glGetUniformLocation(PointLightProgram, "Intensity");
    SourceRadID = glGetUniformLocation(PointLightProgram, "SourceRad");

    bReady = true;
}

PointLight::~PointLight()
{

}



void PointLight::set_position(vec3 _position)
{
    ATransform.set_position(_position);
}


vec3 PointLight::GetIntensity()
{
    return Intensity;
}

void PointLight::SetIntensity(vec3 Int)
{
    Intensity = Int;
}

void PointLight::SetClampRad(float ClampRad_)
{
    ClampRad = ClampRad_;
}

void PointLight::SetSourceRad(float Rad)
{
    SourceRad = Rad;
}

ShadowMap * PointLight::getShadowMap()
{
    return lShadowMap;
}

void PointLight::tick(float delta)
{

}

void PointLight::GameStart()
{

}


void PointLight::BindShadowMapForDrawing(unsigned int face, GLuint program)
{
    if(!bShadows || !bReady){return;}
    lShadowMap->BindShadowMapForDrawing(ATransform.get_position(), SourceRad, ClampRad, face, program);
}

void PointLight::DrawLight(GBuffer InputGBuffer, mat4 MVP)
{
    if(!bReady){return;}
    InputGBuffer.BindGBufferForReading();
    //Disables depth buffer writing
    glDepthMask(GL_FALSE);

    //Sets up the stencil buffer as described here: http://ogldev.atspace.co.uk/
    glEnable(GL_STENCIL_TEST);
    glClear(GL_STENCIL_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glStencilFunc(GL_ALWAYS, 0, 0);

    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

    //Creates a transform matrix for the bounding volume sphere based on MVP and Light's position
    glUseProgram(SphereProg);

    PLightBSphere.SetRad(ClampRad);
    PLightBSphere.SetPos(ATransform.get_position());
    PLightBSphere.Draw(MVP, SphereProg);

    /************************************************************************************************/

    glUseProgram(PointLightProgram);
    glUniform3fv(PositionID , 1, value_ptr(ATransform.get_position()));
    glUniform3fv(IntensityID, 1, value_ptr(Intensity));
    glUniform1f(SourceRadID, SourceRad);
    InputGBuffer.BindGBufferForLighting(PointLightProgram);

    glStencilFunc(GL_NOTEQUAL, 0, 0xFF);

    //Enables blending for cumulating light and disable
    //deth test(we don't need it since we will use the just created stencil buffer)
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);

    if(bShadows)
    {
        lShadowMap->UseShadowMap(PointLightProgram);
    }
    PLightBSphere.Draw(MVP, PointLightProgram);

    glDisable(GL_BLEND);
    glCullFace(GL_BACK);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
}
