#include"main.h"
#include"tiny_obj_loader.h"
#include"textures.h"
#include"Material.h"

class Mesh
{


    public:

        GLuint ShaderID;

        // Model_Matrix setter
        void Load_BML(std::string File, GLenum DrawType);
        void DrawAll();

    private:

        string name;                            //contains the mesh name
        GLuint VBO, NBO, UVB, LUVB, VAO, IBO;
        unsigned int vertices_count, faces_count;

        /*zmat4 Model_Matrix;
        unsigned int MeshCount;
        std::vector<tinyobj::shape_t> Mesh;
        std::vector<tinyobj::material_t> materials;
        std::vector<GLuint> VBO, UVB, NBO, VAO, IBO;
        std::vector<std::string> Meshname;
        std::map<std::string, unsigned int> meshmap;*/
};



