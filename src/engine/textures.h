#pragma once
#include  "main.h"
#include "../common/texture.hpp"
class Texture
{
private:
	GLuint ID;
	std::string name;
public:
	void LoadTexFromDDS(std::string ImageFile);
	GLuint getID();

};
