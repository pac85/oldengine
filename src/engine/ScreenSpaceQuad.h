#ifndef SCREENSPACEQUAD_H
#define SCREENSPACEQUAD_H
#include"main.h"

class ScreenSpaceQuad
{
    public:
        ScreenSpaceQuad();
        void DrawQuad();
        virtual ~ScreenSpaceQuad();

    protected:

    private:
        GLuint Quad, quadArray;
};

#endif // SCREENSPACEQUAD_H
