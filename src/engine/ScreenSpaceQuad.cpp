#include "ScreenSpaceQuad.h"

ScreenSpaceQuad::ScreenSpaceQuad()
{
    glGenVertexArrays(1, &quadArray);
    glBindVertexArray(quadArray);

    static const GLfloat quad[] = {
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        1.0f,  1.0f, 0.0f,
    };

    glGenBuffers(1, &Quad);
    glBindBuffer(GL_ARRAY_BUFFER, Quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void ScreenSpaceQuad::DrawQuad()
{
    glBindVertexArray(quadArray);
    //sends vertices to the shader and renders
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, Quad);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


ScreenSpaceQuad::~ScreenSpaceQuad()
{
    //dtor
}
