// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <map>
#include<vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
//#include <glfw3.h>
#include<SDL2/SDL.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../common/shader.hpp"

#include "../common/controls.hpp"

#include "../common/texture.hpp"


using namespace glm;
using namespace std;



extern int WindowHeight, WindowWidth;
