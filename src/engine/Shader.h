#pragma once
#include"main.h"
#include"textures.h"
#include<vector>
#include <fstream>
#include <string>


class Shader
 {
 private:
	 std::vector<std::string> ProgTxtNames;
	 std::vector<GLuint> SamplerIDs;
	 std::vector<Texture*> Textures;
	 GLuint shaderID;
 public:
	 
	 GLuint GetShaderID();
	 void MakeShader(const char * vertex_file_path, const char * fragment_file_path);
	 void SetTextures(Texture * textureP, std::string NameInShader);
	 void UseShader();


 };