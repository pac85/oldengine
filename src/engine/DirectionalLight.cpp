#include "DirectionalLight.h"
#include "ScreenSpaceQuad.h"
#define PI 3.14159265f

//The registration macro witch must be in every actor derived class
REGISTER_DEF_TYPE(DirectionalLight);

DirectionalLight::DirectionalLight()
{

}

void DirectionalLight::Initial_params(GLuint _DirLightProgram, unsigned int _ShadowMapRes, bool _bShadows)
{
    DirLightProgram = _DirLightProgram, ShadowMapRes = _ShadowMapRes, bShadows = _bShadows;
    if(bShadows)
    {
        lShadowMap = new ShadowMap(ShadowMapRes, false);
        AngleID = glGetUniformLocation(DirLightProgram, "LightAngle");
    }

    DirectionID  = glGetUniformLocation(DirLightProgram, "light_dir");
    IntensityID  = glGetUniformLocation(DirLightProgram, "intensity");
    VPMIndexID   = glGetUniformLocation(DirLightProgram, "ShadowMapVPM");

    bReady = true;
}

void DirectionalLight::BindShadowMapForDrawing(GLuint Program)
{
    if(!bShadows || !bReady){return;}

    VPM = ortho<float>(-1000,1000,-1000,1000,-1050,500) * lookAt(Direction, vec3(0,0,0), vec3(0,1,0));
    lShadowMap->BindShadowMapForDrawing(VPM, Program);
}

void DirectionalLight::SetIntensity(vec3 Int)
{
    intensity = Int;
}

void DirectionalLight::set_rotation(vec3 _rotation)
{
    ATransform.set_rotation(_rotation);
    Direction = ATransform.get_rotation_quat() * vec3(0.0, 1.0, 0.0);
}

void DirectionalLight::SetAngle(float Angle)
{
    source_angle = 2 * tan(PI / 360.0f * Angle);
}


void DirectionalLight::tick(float delta)
{

}

void DirectionalLight::GameStart()
{

}


void DirectionalLight::DrawLight(GBuffer InputGBuffer)
{
    if(!bReady){return;}
    glUseProgram(DirLightProgram);

    InputGBuffer.BindGBufferForLighting(DirLightProgram);

    //enables blend so light can be cumulated
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    if(bShadows)
    {
        lShadowMap->UseShadowMap(DirLightProgram);
        glUniform1f(AngleID, source_angle);
    }

    //sends values to program
    glUniform3fv(DirectionID , 1, value_ptr(Direction));
    glUniform3fv(IntensityID, 1, value_ptr(intensity));
    glUniformMatrix4fv(VPMIndexID, 1, GL_FALSE, &VPM[0][0]);

    if(bShadows)
        lShadowMap->UseShadowMap(DirLightProgram);

    ScreenSpaceQuad t_quad;
    //draws a screen space quad to execute the shader
    t_quad.DrawQuad();

    glDisable(GL_BLEND);
}




DirectionalLight::~DirectionalLight()
{
    //dtor
}
