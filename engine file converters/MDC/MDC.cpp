#include"BML.h"
#include <fstream>
#include <iostream>
using namespace std;


void write_materials(string output_path, vector<tinyobj::material_t> & in_mat)
{
    ofstream output;

    for(unsigned int i = 0;i < in_mat.size();i++)
    {
        output.open(output_path + in_mat[i].name + ".tmt", ios::out );

        //diffuse
        if(!in_mat[i].diffuse_texname.empty())
        {
            output << "-diff -text " << in_mat[i].diffuse_texname << endl;
        }
        else
        {
            output << "-diff -unif " << in_mat[i].diffuse[0] << " " << in_mat[i].diffuse[1] << " " << in_mat[i].diffuse[2] << endl;
        }
        //specular
        if(!in_mat[i].specular_texname.empty())
        {
            output << "-spec -text " << in_mat[i].specular_texname << endl;
        }
        else
        {
            output << "-spec -unif " << in_mat[i].specular[0] << " " << in_mat[i].specular[1] << " " << in_mat[i].specular[2] << endl;
        }
        //roughness specular highlight is used since obj hasn't roughness
        if(!in_mat[i].specular_highlight_texname.empty())
        {
            output << "-rough -text " << in_mat[i].specular_highlight_texname << endl;
        }
        else
        {
            output << "-rough -unif " << in_mat[i].shininess << endl;
        }
        //normal
        if(!in_mat[i].bump_texname.empty())
        {
            output << "-norm -text " << in_mat[i].bump_texname << endl;
        }

        output << "-emi -unif " << in_mat[i].emission[0] << " " << in_mat[i].emission[1] << " " << in_mat[i].emission[2] << endl;

        if(in_mat[i].unknown_parameter.count("dm") != 0)
        {
            output << "-d/m -text " << in_mat[i].unknown_parameter["dm"] << endl;
        }

        output.close();
    }

}

void obj_to_mdc(string inputfile, string output_path, string model_name,string & err)
{
    //converts all the meshes to bml

    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    obj_to_bml(inputfile, output_path, err, shapes, materials);
    write_materials(output_path, materials);

    ofstream output;
    output.open(output_path + model_name + ".mdc", ios::out );

    for(unsigned int i = 0;i < shapes.size();i++)
    {
        string material_path = materials[ shapes[i].mesh.material_ids[0] ].name + ".tmt";
        output << shapes[i].name + ".bml " << material_path << endl;
    }
    output.close();
}

int main(int argc, char* argv[])
{
    if(argc < 4)
    {
        cout<<"usage: mdc /path/to/.obj output/path/ model-name"<< endl;
        return 1;
    }

    string err;
    obj_to_mdc(argv[1], argv[2], argv[3], err);

    if (!err.empty())
    {
      std::cerr << err << std::endl;
    }



    return 0;
}

