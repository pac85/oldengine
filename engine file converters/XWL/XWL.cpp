#include"BML.h"
#include <fstream>
#include"rapidxml/rapidxml.hpp"
#include"rapidxml/rapidxml_print.hpp"
#include <sstream>
using namespace std;
using namespace rapidxml;

void obj_to_xwl(string inputfile, string output_path, string map_name, string err)
{
    //converts all the meshes to bml
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    obj_to_bml(inputfile, output_path, err, shapes, materials);

    //creates an xml document and adds the version node
    xml_document<> world;
    xml_node<>* decl = world.allocate_node(node_declaration);
    decl->append_attribute(world.allocate_attribute("version", "1.0"));
    decl->append_attribute(world.allocate_attribute("encoding", "utf-8"));
    world.append_node(decl);

    //creates the root node with the map_name has name
    xml_node<>* root = world.allocate_node(node_element, map_name.c_str());
    world.append_node(root);

    //adds a children node for each mesh

    vector<string> values;
    for(unsigned int i = 0;i < shapes.size();i++)
    {
        xml_node<> * temp = world.allocate_node(node_element, "Actor");
        temp->append_attribute(world.allocate_attribute("class", "mesh_actor"));
        temp->append_attribute(world.allocate_attribute("position", "0.0 0.0 0.0"));
        temp->append_attribute(world.allocate_attribute("rotation", "0.0 0.0 0.0"));

        string temp_val = shapes[i].name;
        temp_val.append(".bml");
        values.push_back(temp_val);

        temp->value(values[i].c_str());
        root->append_node(temp);
    }

    //outputs to a file
    string file_name =output_path + map_name;
    file_name.append(".xwl");

    ofstream output_file(file_name.c_str());
    output_file << world;
    output_file.close();
    world.clear();
}

int main(int argc, char* argv[])
{

    string err;
    obj_to_xwl("sponza.obj", "test/", "test", err);

    return 0;
}


/*****
    typedef struct {
      std::string name;
      mesh_t mesh;
    } shape_t;

    typedef struct {
      std::vector<float> positions;
      std::vector<float> normals;
      std::vector<float> texcoords;
      std::vector<unsigned int> indices;
      std::vector<unsigned char>
          num_vertices;              // The number of vertices per face. Up to 255.
      std::vector<int> material_ids; // per-face material ID
      std::vector<tag_t> tags;       // SubD tag
    } mesh_t;
    **/
