#include<iostream>
#include<vector>
#include"tiny_obj_loader.h"

bool obj_to_bml(std::string inputfile, std::string output_path, std::string err, std::vector<tinyobj::shape_t> & shapes, std::vector<tinyobj::material_t> materials);
