#define TINYOBJLOADER_IMPLEMENTATION
#include"BML.h"
#include <fstream>

using namespace std;

union
{
    float f;
    int32_t i;
    unsigned char bytes[4];
} to_byte;

union
{
    int16_t i;
    unsigned char bytes[2];
}int_to_char;

void char_cpy(char source[], char dest[], int n, int offset)
{
    for(int i = 0;i < n;i++)
    {
        dest[i + offset] = source[i];
    }
}

struct HexCharStruct
{
  unsigned char c;
  HexCharStruct(unsigned char _c) : c(_c) { }
};

inline std::ostream& operator<<(std::ostream& o, const HexCharStruct& hs)
{
  return (o << std::hex << (int)hs.c);
}

inline HexCharStruct hex(unsigned char _c)
{
  return HexCharStruct(_c);
}



bool obj_to_bml(string inputfile, string output_path, string err, vector<tinyobj::shape_t> & shapes, vector<tinyobj::material_t> materials)
{

    tinyobj::LoadObj(shapes, materials, err, inputfile.c_str());

    if (!err.empty())
    { // `err` may contain warning message.
        std::cerr << err << std::endl;
    }

    /*****
    typedef struct {
      std::string name;
      mesh_t mesh;
    } shape_t;

    typedef struct {
      std::vector<float> positions;
      std::vector<float> normals;
      std::vector<float> texcoords;
      std::vector<unsigned int> indices;
      std::vector<unsigned char>
          num_vertices;              // The number of vertices per face. Up to 255.
      std::vector<int> material_ids; // per-face material ID
      std::vector<tag_t> tags;       // SubD tag
    } mesh_t;
    **/

    ofstream out_BML;
    //iterates over all the objects in the obj file
    for(unsigned int i = 0;i < shapes.size();i++)
    {
        string out_name = output_path;
        out_name.append(shapes[i].name);
        out_name.append(".bml");

        out_BML.open(out_name.c_str(), ios::out | ios::binary);

        cout << "processing "<<shapes[i].name<<endl;

        const char * model_name = shapes[i].name.c_str();
        //writes the header

        //calculates header size
        //the header size is always 11 + name string size  bytes
        int header_size = 11;
        header_size += shapes[i].name.size();


        char * header = new char[header_size];

        //the header always starts with this bit
        header[0] = (char)0xF9;

        //writes the 16 bit integer for the name length
        int_to_char.i = header_size - 11;
        header[1] = int_to_char.bytes[0];
        header[2] = int_to_char.bytes[1];

        //writes model name
        char_cpy((char*)model_name, header, header_size - 11, 3);

        //writes the two 32 bit integers for vertices count and index count
        to_byte.i = shapes[i].mesh.positions.size() / 3;
        char_cpy((char*)to_byte.bytes, header, 4, header_size - 8);
        to_byte.i = shapes[i].mesh.indices.size() / 3;
        char_cpy((char*)to_byte.bytes, header, 4, header_size - 4);

        out_BML.write(header, header_size);

        for(unsigned int y = 0;y < shapes[i].mesh.positions.size(); y++)
        {
             to_byte.f = shapes[i].mesh.positions[y];
             out_BML.write((char*)to_byte.bytes, 4);
        }
        for(unsigned int y = 0;y < shapes[i].mesh.normals.size(); y++)
        {
             to_byte.f = shapes[i].mesh.normals[y];
             out_BML.write((char*)to_byte.bytes, 4);
        }
        if(shapes[i].mesh.texcoords.size() == 0)
        {
            cout<<"The model needs to have UV coordinates"<<endl;
            return false;
        }
        for(unsigned int y = 0;y < shapes[i].mesh.texcoords.size(); y++)
        {
             to_byte.f = shapes[i].mesh.texcoords[y];
             out_BML.write((char*)to_byte.bytes, 4);
        }
        for(unsigned int y = 0;y < shapes[i].mesh.texcoords.size(); y++)
        {
             to_byte.f = shapes[i].mesh.texcoords[y];
             out_BML.write((char*)to_byte.bytes, 4);
        }
        for(unsigned int y = 0;y < shapes[i].mesh.indices.size(); y++)
        {
             to_byte.i = shapes[i].mesh.indices[y];
             out_BML.write((char*)to_byte.bytes, 4);
        }

        out_BML.close();

       // for(unsigned int i = 0;i < shapes[i].mesh.positions)
    }
    return true;
}
