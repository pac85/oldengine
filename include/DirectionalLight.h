#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H
#include"main.h"
#include"GBuffer.h"
#include"ShadowMap.h"
#include"Actor.h"


class DirectionalLight : public Actor
{
    public:
        DirectionalLight();
        void Initial_params(GLuint, unsigned int, bool);
        void BindShadowMapForDrawing(GLuint);
        void DrawLight(GBuffer InputGBuffer);
        void SetIntensity(vec3 Int);
        void set_rotation(vec3 _rotation);
        void SetAngle(float Angle);
        virtual ~DirectionalLight();

    protected:

    private:
        //The registration macro witch must be in every actor derived class
        REGISTER_DEC_TYPE(DirectionalLight);

        bool bReady,        //set to true only once Initial_params is called
         bShadows;
        unsigned int ShadowMapRes;
        GLuint DirLightProgram, DirectionID, IntensityID, VPMIndexID, AngleID;
        ShadowMap * lShadowMap;
        vec3 Direction, intensity;
        float source_angle;
        mat4 VPM;

        void tick(float delta);
        void GameStart();
};

#endif // DIRECTIONALLIGHT_H
